package prefixnamer

/*
Provides utility to store CIDR blocks with a name/number
and then perform ip lookup tests against the store.

To create a new instance of the library:

pfxlist := NewDualStackPrefixNamer()

			_, network, _ := net.ParseCIDR("192.168.0.0/24")
			pfxlist.Insert(NewBasicPrefixEntry(*network))
			pfxlist.Remove(network)

Find if network is in the list (returns first match)
			containsBool, err := pfxlist.Contains(net.ParseIP("192.168.0.1"))
Used for route lookups, i.e. more specific than Contains()
			containsBool, err := pfxlist.ContainsBestMatch(net.ParseIP("192.168.0.1"))
Get all entries:
			entries, err := pfxlist.ContainingNetworks(net.ParseIP("192.168.0.1"))
*/

import (
	"net"
)

// NewIPv4PrefixNamer returns a new IPv4 address family Prefix Namer
func NewIPv4PrefixNamer() PrefixList {
	return newPrefixTree(IPv4)
}

// NewIPv6PrefixNamer returns a new IPv6 address family Prefix Namer
func NewIPv6PrefixNamer() PrefixList {
	return newPrefixTree(IPv6)
}

// NewBasicPrefixEntry returns a basic PrefixEntry that only stores the network
// itself.
func NewBasicPrefixEntry(thisip net.IPNet) PrefixEntry {
	return &BasicPrefixEntry{
		IPNet: thisip,
		Name:  "",
		Num64: 0,
	}
}
