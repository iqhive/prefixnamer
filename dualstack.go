package prefixnamer

import (
	"net"
)

type dualStackPrefixNamerFactory func(IPVersion) PrefixList

type dualStackPrefixName struct {
	IPv4PrefixNamer PrefixList
	IPv6PrefixNamer PrefixList
}

// NewDualStackPrefixNamer returns a dualStackPrefixName that supports both IPv4 and IPv6
// using the path compressed trie implemention.
func NewDualStackPrefixNamer() PrefixList {
	return newDualStackPrefixNamer(newPrefixTree)
}

func newDualStackPrefixNamer(factory dualStackPrefixNamerFactory) PrefixList {
	return &dualStackPrefixName{
		IPv4PrefixNamer: factory(IPv4),
		IPv6PrefixNamer: factory(IPv6),
	}
}

func (v *dualStackPrefixName) Insert(entry PrefixEntry) error {
	network := entry.GetIPNet()
	pfxlist, err := v.getIPStackSpecificPrefixName(network.IP)
	if err != nil {
		return err
	}
	return pfxlist.Insert(entry)
}

func (v *dualStackPrefixName) Remove(network net.IPNet) (PrefixEntry, error) {
	pfxlist, err := v.getIPStackSpecificPrefixName(network.IP)
	if err != nil {
		return nil, err
	}
	return pfxlist.Remove(network)
}

func (v *dualStackPrefixName) Contains(ip net.IP) (bool, PrefixEntry, error) {
	pfxlist, err := v.getIPStackSpecificPrefixName(ip)
	if err != nil {
		return false, nil, err
	}
	return pfxlist.Contains(ip)
}

func (v *dualStackPrefixName) ContainsBestMatch(ip net.IP) (bool, PrefixEntry, error) {
	pfxlist, err := v.getIPStackSpecificPrefixName(ip)
	if err != nil {
		return false, nil, err
	}
	return pfxlist.ContainsBestMatch(ip)
}

func (v *dualStackPrefixName) ContainingNetworks(ip net.IP) ([]PrefixEntry, error) {
	pfxlist, err := v.getIPStackSpecificPrefixName(ip)
	if err != nil {
		return nil, err
	}
	return pfxlist.ContainingNetworks(ip)
}

func (v *dualStackPrefixName) CoveredNetworks(network net.IPNet) ([]PrefixEntry, error) {
	pfxlist, err := v.getIPStackSpecificPrefixName(network.IP)
	if err != nil {
		return nil, err
	}
	return pfxlist.CoveredNetworks(network)
}

func (v *dualStackPrefixName) getIPStackSpecificPrefixName(ip net.IP) (PrefixList, error) {
	if ip.To4() != nil {
		return v.IPv4PrefixNamer, nil
	}
	if ip.To16() != nil {
		return v.IPv6PrefixNamer, nil
	}
	return nil, ErrInvalidNetworkNumberInput
}
