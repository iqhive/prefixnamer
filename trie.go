package prefixnamer

import (
	"fmt"
	"net"
	"strings"
)

// prefixTrie is a path-compressed (PC) trie implementation of the
// PrefixList interface inspired by this blog post:
// https://vincent.bernat.im/en/blog/2017-ipv4-route-lookup-linux
//
// CIDR blocks are stored using a prefix tree structure where each node has its
// parent as prefix, and the path from the root node represents current CIDR
// block.
//
// For IPv4, the trie structure guarantees max depth of 32 as IPv4 addresses are
// 32 bits long and each bit represents a prefix tree starting at that bit. This
// property also guarantees constant lookup time in Big-O notation.
//
// Path compression compresses a string of node with only 1 child into a single
// node, decrease the amount of lookups necessary during containment tests.
//
// Level compression dictates the amount of direct children of a node by
// allowing it to handle multiple bits in the path.  The heuristic (based on
// children population) to decide when the compression and decompression happens
// is outlined in the prior linked blog, and will be experimented with in more
// depth in this project in the future.
//
// Note: Can not insert both IPv4 and IPv6 network addresses into the same
// prefix trie, use DualStackPrefixNamer wrapper instead.
//
// TODO: Implement level-compressed component of the LPC trie.
type prefixTrie struct {
	parent   *prefixTrie
	children []*prefixTrie

	numBitsSkipped uint
	numBitsHandled uint

	network Network
	entry   PrefixEntry
}

// newPrefixTree creates a new prefixTrie.
func newPrefixTree(version IPVersion) PrefixList {
	_, rootNet, _ := net.ParseCIDR("0.0.0.0/0")
	if version == IPv6 {
		_, rootNet, _ = net.ParseCIDR("0::0/0")
	}
	return &prefixTrie{
		children:       make([]*prefixTrie, 2, 2),
		numBitsSkipped: 0,
		numBitsHandled: 1,
		network:        NewNetwork(*rootNet),
	}
}

func newPathprefixTrie(network Network, numBitsSkipped uint) *prefixTrie {
	version := IPv4
	if len(network.Number) == IPv6Uint32Count {
		version = IPv6
	}
	path := newPrefixTree(version).(*prefixTrie)
	path.numBitsSkipped = numBitsSkipped
	path.network = network.Masked(int(numBitsSkipped))
	return path
}

func newEntryTrie(network Network, entry PrefixEntry) *prefixTrie {
	ones, _ := network.IPNet.Mask.Size()
	leaf := newPathprefixTrie(network, uint(ones))
	leaf.entry = entry
	return leaf
}

// Insert inserts a PrefixEntry into prefix trie.
func (p *prefixTrie) Insert(entry PrefixEntry) error {
	network := entry.GetIPNet()
	return p.insert(NewNetwork(network), entry)
}

// Remove removes PrefixEntry identified by given network from trie.
func (p *prefixTrie) Remove(network net.IPNet) (PrefixEntry, error) {
	return p.remove(NewNetwork(network))
}

// Contains returns boolean indicating whether given ip is contained in any
// of the inserted networks.
func (p *prefixTrie) Contains(ip net.IP) (bool, PrefixEntry, error) {
	nn := NewNetworkNumber(ip)
	if nn == nil {
		return false, nil, ErrInvalidNetworkNumberInput
	}
	return p.containsBiggestMatch(nn)
}

// ContainsBestMatch returns boolean indicating whether given ip is contained in any
// of the inserted networks, and uses the best possible route match (shortest prefix).
func (p *prefixTrie) ContainsBestMatch(ip net.IP) (bool, PrefixEntry, error) {
	nn := NewNetworkNumber(ip)
	if nn == nil {
		return false, nil, ErrInvalidNetworkNumberInput
	}
	return p.containsBestMatch(nn)
}

// ContainingNetworks returns the list of PrefixEntry(s) the given ip is
// contained in in ascending prefix order.
func (p *prefixTrie) ContainingNetworks(ip net.IP) ([]PrefixEntry, error) {
	nn := NewNetworkNumber(ip)
	if nn == nil {
		return nil, ErrInvalidNetworkNumberInput
	}
	return p.containingNetworks(nn)
}

// CoveredNetworks returns the list of PrefixEntry(s) the given ipnet
// covers.  That is, the networks that are completely subsumed by the
// specified network.
func (p *prefixTrie) CoveredNetworks(network net.IPNet) ([]PrefixEntry, error) {
	net := NewNetwork(network)
	return p.coveredNetworks(net)
}

// String returns string representation of trie, mainly for visualization and
// debugging.
func (p *prefixTrie) String() string {
	children := []string{}
	padding := strings.Repeat("| ", p.level()+1)
	for bits, child := range p.children {
		if child == nil {
			continue
		}
		childStr := fmt.Sprintf("\n%s%d--> %s", padding, bits, child.String())
		children = append(children, childStr)
	}
	return fmt.Sprintf("%s (target_pos:%d:has_entry:%t)%s", p.network,
		p.targetBitPosition(), p.hasEntry(), strings.Join(children, ""))
}

// containsBestMatch returns the shortest route prefix that matches
func (p *prefixTrie) containsBestMatch(number NetworkNumber) (bool, PrefixEntry, error) {
	var result PrefixEntry
	if !p.network.Contains(number) {
		return false, result, nil
	}
	if p.hasEntry() {
		result = p.entry
	}
	if p.targetBitPosition() < 0 {
		return false, result, nil
	}
	bit, err := p.targetBitFromIP(number)
	if err != nil {
		return false, nil, err
	}
	child := p.children[bit]
	if child != nil {
		ranges, err := child.containingNetworks(number)
		if err != nil {
			return false, nil, err
		}
		if len(ranges) > 0 {
			result = ranges[len(ranges)-1]
		} else {
			return false, nil, nil
		}
	}
	return true, result, nil
}

func (p *prefixTrie) containsBiggestMatch(number NetworkNumber) (bool, PrefixEntry, error) {
	if !p.network.Contains(number) {
		return false, nil, nil
	}
	if p.hasEntry() {
		return true, p.entry, nil
	}
	if p.targetBitPosition() < 0 {
		return false, nil, nil
	}
	bit, err := p.targetBitFromIP(number)
	if err != nil {
		return false, nil, err
	}
	child := p.children[bit]
	if child != nil {
		return child.containsBiggestMatch(number)
	}
	return false, nil, nil
}

func (p *prefixTrie) containingNetworks(number NetworkNumber) ([]PrefixEntry, error) {
	results := []PrefixEntry{}
	if !p.network.Contains(number) {
		return results, nil
	}
	if p.hasEntry() {
		results = []PrefixEntry{p.entry}
	}
	if p.targetBitPosition() < 0 {
		return results, nil
	}
	bit, err := p.targetBitFromIP(number)
	if err != nil {
		return nil, err
	}
	child := p.children[bit]
	if child != nil {
		ranges, err := child.containingNetworks(number)
		if err != nil {
			return nil, err
		}
		if len(ranges) > 0 {
			if len(results) > 0 {
				results = append(results, ranges...)
			} else {
				results = ranges
			}
		}
	}
	return results, nil
}

func (p *prefixTrie) coveredNetworks(network Network) ([]PrefixEntry, error) {
	var results []PrefixEntry
	if network.Covers(p.network) {
		for entry := range p.walkDepth() {
			results = append(results, entry)
		}
	} else if p.targetBitPosition() >= 0 {
		bit, err := p.targetBitFromIP(network.Number)
		if err != nil {
			return results, err
		}
		child := p.children[bit]
		if child != nil {
			return child.coveredNetworks(network)
		}
	}
	return results, nil
}

func (p *prefixTrie) insert(network Network, entry PrefixEntry) error {
	if p.network.Equal(network) {
		// Merge the []string and []int64 slices
		if p.entry != nil {
			if list := entry.GetNames(); list != nil {
				entry.AddNames(p.entry.GetNames())
			}
			if list := entry.GetNums64(); list != nil {
				entry.AddNums64(p.entry.GetNums64())
			}
		}
		p.entry = entry
		return nil
	}
	bit, err := p.targetBitFromIP(network.Number)
	if err != nil {
		return err
	}
	child := p.children[bit]
	if child == nil {
		return p.insertPrefix(bit, newEntryTrie(network, entry))
	}

	lcb, err := network.LeastCommonBitPosition(child.network)
	if err != nil {
		return err
	}
	if int(lcb) > child.targetBitPosition()+1 {
		child = newPathprefixTrie(network, p.totalNumberOfBits()-lcb)
		err := p.insertPrefix(bit, child)
		if err != nil {
			return err
		}
	}
	return child.insert(network, entry)
}

func (p *prefixTrie) insertPrefix(bits uint32, prefix *prefixTrie) error {
	child := p.children[bits]
	if child != nil {
		prefixBit, err := prefix.targetBitFromIP(child.network.Number)
		if err != nil {
			return err
		}
		prefix.insertPrefix(prefixBit, child)
	}
	p.children[bits] = prefix
	prefix.parent = p
	return nil
}

func (p *prefixTrie) remove(network Network) (PrefixEntry, error) {
	if p.hasEntry() && p.network.Equal(network) {
		entry := p.entry
		if p.childrenCount() > 1 {
			p.entry = nil
		} else {
			// Has 0 or 1 child.
			parentBits, err := p.parent.targetBitFromIP(network.Number)
			if err != nil {
				return nil, err
			}
			var skipChild *prefixTrie
			for _, child := range p.children {
				if child != nil {
					skipChild = child
					break
				}
			}
			p.parent.children[parentBits] = skipChild
		}
		return entry, nil
	}
	bit, err := p.targetBitFromIP(network.Number)
	if err != nil {
		return nil, err
	}
	child := p.children[bit]
	if child != nil {
		return child.remove(network)
	}
	return nil, nil
}

func (p *prefixTrie) childrenCount() int {
	count := 0
	for _, child := range p.children {
		if child != nil {
			count++
		}
	}
	return count
}

func (p *prefixTrie) totalNumberOfBits() uint {
	return BitsPerUint32 * uint(len(p.network.Number))
}

func (p *prefixTrie) targetBitPosition() int {
	return int(p.totalNumberOfBits()-p.numBitsSkipped) - 1
}

func (p *prefixTrie) targetBitFromIP(n NetworkNumber) (uint32, error) {
	// This is a safe uint boxing of int since we should never attempt to get
	// target bit at a negative position.
	return n.Bit(uint(p.targetBitPosition()))
}

func (p *prefixTrie) hasEntry() bool {
	return p.entry != nil
}

func (p *prefixTrie) level() int {
	if p.parent == nil {
		return 0
	}
	return p.parent.level() + 1
}

// walkDepth walks the trie in depth order, for unit testing.
func (p *prefixTrie) walkDepth() <-chan PrefixEntry {
	entries := make(chan PrefixEntry)
	go func() {
		if p.hasEntry() {
			entries <- p.entry
		}
		childEntriesList := []<-chan PrefixEntry{}
		for _, trie := range p.children {
			if trie == nil {
				continue
			}
			childEntriesList = append(childEntriesList, trie.walkDepth())
		}
		for _, childEntries := range childEntriesList {
			for entry := range childEntries {
				entries <- entry
			}
		}
		close(entries)
	}()
	return entries
}
