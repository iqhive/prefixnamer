package prefixnamer

import (
	"net"
)

// PrefixList is an interface for cidr block containment lookups.
type PrefixList interface {
	Insert(entry PrefixEntry) error
	Remove(network net.IPNet) (PrefixEntry, error)
	Contains(ip net.IP) (bool, PrefixEntry, error)
	ContainsBestMatch(ip net.IP) (bool, PrefixEntry, error)
	ContainingNetworks(ip net.IP) ([]PrefixEntry, error)
	CoveredNetworks(network net.IPNet) ([]PrefixEntry, error)
}

// NetworkNumber represents an IP address using uint32 as internal storage.
// IPv4 usings 1 uint32, while IPv6 uses 4 uint32.
type NetworkNumber []uint32

// IPVersion is version of IP address.
type IPVersion string

// Network represents a block of network numbers, also known as CIDR.
type Network struct {
	net.IPNet
	Number NetworkNumber
	Mask   NetworkNumberMask
}

// Helper constants.
const (
	IPv4Uint32Count = 1
	IPv6Uint32Count = 4

	BitsPerUint32 = 32
	BytePerUint32 = 4

	IPv4 IPVersion = "IPv4"
	IPv6 IPVersion = "IPv6"
)

// PrefixEntry is an interface for insertable entry into a PrefixList.
type PrefixEntry interface {
	GetName() string
	GetNames() []string
	AddName(string)
	AddNames([]string)
	GetNum64() int64
	GetNums64() []int64
	AddNum64(int64)
	AddNums64([]int64)
	GetIPNet() net.IPNet
}

type BasicPrefixEntry struct {
	IPNet  net.IPNet
	Name   string
	Names  []string
	Num64  int64
	Nums64 []int64
}

func (b *BasicPrefixEntry) GetIPNet() net.IPNet {
	return b.IPNet
}
func (b *BasicPrefixEntry) GetName() string {
	return b.Name
}
func (b *BasicPrefixEntry) GetNames() []string {
	return b.Names
}
func (b *BasicPrefixEntry) AddName(newitem string) {
	b.Names = MergeNames(b.Names, []string{newitem})
}
func (b *BasicPrefixEntry) AddNames(newitems []string) {
	b.Names = MergeNames(b.Names, newitems)
}
func MergeNames(oldvals []string, newvals []string) []string {
	if newvals == nil {
		return oldvals
	}
	if oldvals == nil {
		return newvals
	}
	for n := range newvals {
		found := false
		for e := range oldvals {
			if newvals[n] == oldvals[e] {
				found = true
				break
			}
		}
		if !found {
			oldvals = append(oldvals, newvals[n])
		}
	}
	return oldvals
}
func (b *BasicPrefixEntry) GetNum64() int64 {
	return b.Num64
}
func (b *BasicPrefixEntry) GetNums64() []int64 {
	return b.Nums64
}
func (b *BasicPrefixEntry) AddNum64(newitem int64) {
	b.Nums64 = MergeNums64(b.Nums64, []int64{newitem})
}
func (b *BasicPrefixEntry) AddNums64(newitems []int64) {
	b.Nums64 = MergeNums64(b.Nums64, newitems)
}
func MergeNums64(oldvals []int64, newvals []int64) []int64 {
	if newvals == nil {
		return oldvals
	}
	if oldvals == nil {
		return newvals
	}
	for n := range newvals {
		found := false
		for e := range oldvals {
			if newvals[n] == oldvals[e] {
				found = true
				break
			}
		}
		if !found {
			oldvals = append(oldvals, newvals[n])
		}
	}
	return oldvals
}
